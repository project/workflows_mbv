CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration



INTRODUCTION
------------

Current Maintainer: Nasser Tijani nassertijani@gmail.com

This module rovide a basic view for listing moderated content and corresponding
actions. This view can be extended to meet specific customer needs.

![
Moderation view
](https://www.drupal.org/files/project-images/moderation_view_0.png)

REQUIREMENTS
------------

Views, Workflows, Content Moderation (Drupal core modules).

INSTALLATION
------------

```
$ composer require 'drupal/workflows_mbv'
```

CONFIGURATION
-------------

For the moment the module does not require any particular configurations.
