<?php

namespace Drupal\Tests\workflows_mbv\Functional;

use Drupal\Tests\basic_auth\Traits\BasicAuthTestTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group workflows_mbv
 */
class LoadTest extends BrowserTestBase {

  use BasicAuthTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'user',
    'node',
    'views',
    'basic_auth',
    'workflows',
    'content_moderation',
    'workflows_mbv',
  ];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser([], 'admin_test', TRUE);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the content-moderation page loads with a 200 response.
   */
  public function testLoad() {
    $this->drupalGet('/content-moderation');
    $this->assertResponse('200');
  }

}
